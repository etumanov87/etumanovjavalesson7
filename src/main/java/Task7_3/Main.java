package Task7_3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Для структуры данных ArrayList выгоднее выполнять следующие операции:
 * 1. Вывод элементов в консоль (Быстрее потому, что у элементов есть индексация)
 * 2. Изменение элементов (Быстрее потому, что обратиться к нужному элементу быстрее по индексу, чем по ссылке)
 * Для структуры данных LinkedList выгоднее выполнять следующие операции:
 * 1. Добавление элементов (Быстрее потому, что элементы лежать в списке по ссылкам, а не по индексам)
 * 2. Удаление элементов (Быстрее потому, изменения ссылок у элементов происходить быстрее, чем изменение индексов)
 */

public class Main {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < 1_000_000; i++) {
            arrayList.add(i);
            linkedList.add(i);
        }

        long timeBefore = System.currentTimeMillis();
//        addElementBegin(arrayList, 100); //Время выполнения операции : 1 миллисекунд
//        addElementBegin(linkedList, 100); //Время выполнения операции : 0 миллисекунд
//        print100ElementMiddleList(arrayList,500000, 500101);//Время выполнения операции : 4 миллисекунд
//        print100ElementMiddleList(linkedList,500000, 500101); //Время выполнения операции : 8 миллисекунд
//        delete1000ElementAtFirstList(arrayList); //Время выполнения операции : 2 миллисекунд
//        delete1000ElementAtFirstList(linkedList);//Время выполнения операции : 1 миллисекунд
//        change100ElementMiddleList(arrayList,500000, 500101); //Время выполнения операции : 126 миллисекунд
//        change100ElementMiddleList(linkedList,500000, 500101);//Время выполнения операции : 654 миллисекунд
        long timeAfter = System.currentTimeMillis();

        long result = timeAfter - timeBefore;
        System.out.println(String.format("Время выполнения операции : %d миллисекунд", result));

    }

    /**
     * 1. метод который принимает в качестве параметра список и осуществляет добавление элементов в начало списка.
     */
    public static void addElementBegin(List<Integer> list, int element) {
        list.add(0, element);
    }

    /**
     * 2. метод, который принимает в качестве параметра список и печатает на экран 100 элементов из середины (например с 500000го по 500100й)
     */
    public static void print100ElementMiddleList(List<Integer> list, int start, int end) {
        if (start < 0 || end >= list.size()) {
            throw new IllegalArgumentException("Неверные входные параметры");
        }
        for (Integer element : list.subList(start, end)) {
            System.out.println(element);
        }
    }

    /**
     * 3. метод, который удаляет 1000 первых элементов из списка
     */
    public static void delete1000ElementAtFirstList(List<Integer> list) {
        list.subList(0, 1000).clear();
    }

    /**
     * 4. метод, который устанавливает новые значения для 100 элементов из середины (например с 500000го по 500100й)
     */
    public static void change100ElementMiddleList(List<Integer> list, int start, int end) {
        if (start < 0 || end >= list.size() || end - start < 101) {
            throw new IllegalArgumentException("Неверные входные параметры");
        }
        for (Integer element : list.subList(start, end)) {
            list.set(list.indexOf(element), element + 5);
        }
    }
}
