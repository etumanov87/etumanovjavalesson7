package Task7_2;

import java.util.ArrayList;
import java.util.List;

public class Ball {
    static int count = 0;
    static List<String> typeCreateBalls = new ArrayList<>();
    int weight;
    String type;


    public Ball(int weight, String type) {
        this.weight = weight;
        this.type = type;
        count++;
        if(!typeCreateBalls.contains(type)) {
            typeCreateBalls.add(type);
        }
    }

    @Override
    public String toString() {
        return "Ball{" +
                "weight=" + weight +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if(this == object) return true;
        if(object == null) return false;
        Ball another = (Ball) object;
        return this.weight == another.weight && this.type.equals(another.type);
    }

    @Override
    public int hashCode() {
        return this.weight + this.type.length();
    }
}
