package Task7_2;

public class Main {
    public static void main(String[] args) {
        Ball ball1 = new Ball(15, "Football");
        Ball ball2 = new Ball(15, "Football");
        Ball ball3 = new Ball(10, "Basketball");

        System.out.println(ball1.equals(ball2));
        System.out.println(ball1.equals(ball3));
        System.out.println("-----------------------------");
        System.out.println(Ball.typeCreateBalls);
        System.out.println("-----------------------------");
        int b1 = ball1.hashCode();
        int b2 = ball2.hashCode();
        int b3 = ball3.hashCode();
        System.out.println("hashCode ball1 =" + b1);
        System.out.println("hashCode ball1 =" + b2);
        System.out.println("hashCode ball1 =" + b3);

    }
}
