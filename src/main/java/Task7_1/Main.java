package Task7_1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        /**
         * 1. Создать массив цветов (минимум 7 позиций).
         */
        String[] colors = {"красный", "синий", "зеленый", "желтый", "оранжевый", "фиолетовый", "черный"};

        /**
         * 2. Сконвертировать массив в список (ArrayList).
         */
        ArrayList<String> colorsList = new ArrayList<>(Arrays.asList(colors));

        /**
         * 3. Вывести на экран весь список с помощью итератора.
         */
        Iterator<String> iterator = colorsList.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-------------------------");

        /**
         * 4. Отсортировать список по алфавиту.
         */
        Collections.sort(colorsList);
        System.out.println(colorsList);
        System.out.println("-------------------------");

        /**
         * 5. Заменить элементы списка на позициях 1, 3, 5 на аналогичные, но с приставкой + "Dark ".
         */
        colorsList.set(0, "Dark " + colorsList.get(0));
        colorsList.set(2, "Dark " + colorsList.get(2));
        colorsList.set(4, "Dark " + colorsList.get(4));


        /**
         * 6. Вывести на экран весь список с помощью for-each цикла.
         */
        for(String color : colorsList) {
            System.out.println(color);
        }
        System.out.println("-------------------------");

        /**
         * 7. Получить под-список с 1 по 5 элементы включительно.
         */
        System.out.println(colorsList.subList(0, 5));

        System.out.println("-------------------------");

        /**
         * 8. Написать метод, который поменяет местами 1 и 4 элементы межды собой.
         */
        System.out.println(colorsList);
        swapsElements(colorsList, 0, 3);
        System.out.println(colorsList);

        System.out.println("-------------------------");

        /**
         * 9. Получить элемент с индексом 3, сохранить в переменную а1.
         */
        String a1 = colorsList.get(2);
        System.out.println(a1);
        System.out.println("-------------------------");

        /**
         * 10. Проверить, содержится ли объект из переменной а1 в коллекции.
         */
        boolean isElement = colorsList.contains(a1);
        System.out.println(isElement);

        /**
         * 11. Удалить все элементы, содержащие букву 'o'
         */
        colorsList.removeIf(color -> color.toLowerCase().contains("о"));

        System.out.println("-------------------------");

        /**
         *  12. Превратить список в массив.
         */
        String[] arrayColors = colorsList.toArray(new String[0]);
        System.out.println(Arrays.toString(arrayColors));
    }

    public static void swapsElements(ArrayList<String> arrayList, int indexChangeable, int indexReplaceable) {
        String color = arrayList.get(indexChangeable);
        arrayList.set(indexChangeable, arrayList.get(indexReplaceable));
        arrayList.set(indexReplaceable, color);
    }
}
